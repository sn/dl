This repository contails only sources for D/L

* `be.hubzilla.zcponly.20221221.tar.gz` 

### The following files are outdated and should not be used anymore:

* `snh.prod.4.6.2.incl.addon.20200422.tar.gz` 
is based on hubzilla 4.6 (prod) including the addons available for that version.
Contains the modifications for Invite and Register.

To fresh install on the own system, go to the basedir of your site and run
`tar -xvf snh.prod.4.6.2.incl.addon.20200412.tar.gz`

* `snh.prod.4.6.2.diffonly.20200422.tar.gz`  
contains only the files changed between 4.6.0 and 4.6.2
